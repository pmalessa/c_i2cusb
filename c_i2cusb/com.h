/*! \file com.h
    \brief Header of Communication Driver
*/

#ifndef COM_H
#define COM_H

#include <stdio.h>
#include <stdlib.h>

/**Timeout for Reset and Receive Functions, default 1000ms*/
#define TIMEOUT 1000
/**Debug switch to enable Debug messages, default 0*/
//#define DEBUG 0

/**
 * \fn unsigned char COM_Init(unsigned char port)
 * \brief Initialize the Communication Driver
 * \param[in] port COM Port Number found in Windows Device Manager
 * \return 0 successful
 * \return 1 Port not found
 * \return 2 Port open failed
 * \return 3 Port reset error
 */
unsigned char COM_Init(unsigned char port);

/**
 * \fn unsigned char COM_sendByte(unsigned char byte)
 * \brief Send the given byte via COM-Port to the USB-ITS Box
 * \param[in] byte ASCII encoded character
 * \return 0 successful
 * \return 1 not successful, no byte sent
 */
unsigned char COM_sendByte(unsigned char byte);

/**
 * \fn unsigned char COM_getByte(unsigned char* byte)
 * \brief Tries to read a byte from COM-Port from the USB-ITS Box. It blocks for a maximum of TIMEOUT ms (default=1000).
 * \param[in] byte unsigned char pointer to save the byte in
 * \return 0 successful
 * \return 1 Timeout reached
 */
unsigned char COM_getByte(unsigned char* byte);

/**
 * \fn void COM_Flush()
 * \brief Clears the read buffer from any old received characters
 */
void COM_Flush();

/**
 * \fn unsigned char COM_Reset()
 * \brief Tries to reset the USB-ITS Box by sending 'X''X' and waiting for an answer till TIMEOUT ms reached (default=1000).
 * \return 0 successful
 * \return 1 not successful, sending 'X' 'X' failed
 * \return 2 not successful, not 'X' 'X' received
 */
unsigned char COM_Reset();

#endif //COM_H
