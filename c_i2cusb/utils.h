/*! \file utils.h
    \brief Collection of Utility Functions
*/

#ifndef UTILS_H
#define UTILS_H

/**
 * \fn void wait(int ms)
 * \brief Wait for ms milliseconds
 * \param[in] ms milliseconds to wait
 */
void wait(int ms);

#endif //UTILS_H
