/*! \file utils.c
    \brief Implementation of Utility Functions
*/

#include "utils.h"

#ifdef _WIN32
    #include <windows.h>
#else
    #include <unistd.h>
#endif

void wait( int ms )
{   // Pretty crossplatform, both ALL POSIX compliant systems AND Windows
    #ifdef _WIN32
        Sleep( ms );
    #else
        sleep( ms / 1000 );
    #endif
}
