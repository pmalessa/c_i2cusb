/*! \file com.c
    \brief Implementation of Communication Driver
*/

#include <stdio.h>
#include <stdlib.h>
#include "com.h"
#include "rs232/rs232.h"
#include "utils.h"

/**Global variable for Com Port Number*/
unsigned char ComPortNr;

unsigned char COM_Init(unsigned char port)
{
    int status;
    int index = comEnumerate();                         //scan how many COM Ports are available
#ifdef DEBUG
    printf("comEnumerate Nr Ports: %i\n",index);
    for(int i=0;i<index;i++)
    {
        printf("\t%i: %s\n",i,comGetPortName(i));
    }
#endif
    unsigned char ComName[6];
    sprintf((char*)ComName,"COM%d",port);                  //insert Port number in COM name
#ifdef DEBUG
    printf("finding Index by Name: %s\n", ComName);
#endif
    index = comFindPort((char*)ComName);                   //scan for COM Port
    if(index == -1)
    {
#ifdef DEBUG
        printf("Port not found\n");
#endif
        return 1;
    }
#ifdef DEBUG
    printf("opening Port Index:%d\n", index);
#endif
    status = comOpen(index, 38400);                 //open the selected COM Port
    if (status != 1)
    {
#ifdef DEBUG
        printf("Port open failed\n");
#endif
        return 2;
    }
    ComPortNr = index;                              //save the Port number
    if(COM_Reset() != 0)                            //Call Reset to test the communication
    {
#ifdef DEBUG
        printf("Reset Error\n");
#endif
        return 3;
    }
#ifdef DEBUG
    printf("Init successful\n");
#endif
    return 0;
}

unsigned char COM_getByte(unsigned char* byte)
{
    unsigned char buf;
    int status, ms = 0;
    while((ms < TIMEOUT) && ((status = comRead(ComPortNr,(char*)&buf,1)) == 0))   //while timeout not reached AND no byte received
    {
        ms+= 100;
        wait(100);
#ifdef DEBUG
        printf(".");
#endif
    }
    if(status != 0)
    {
#ifdef DEBUG
        printf("Read %d byte: 0x%x\t'%c'\n",status,buf,buf);
#endif
    }
    else
    {
#ifdef DEBUG
        printf("Timeout\n");
#endif
        return 1;
    }

    *byte = buf;
    return 0;
}

unsigned char COM_sendByte(unsigned char byte)
{
    int status;
    status = comWrite(ComPortNr,(char*)&byte,1);
    if (status == 0)
    {
#ifdef DEBUG
        printf("No Byte Sent\n");
#endif
        return 1;
    }
#ifdef DEBUG
    printf("Sent %d byte: 0x%x\t'%c'\n",status,byte,byte);
#endif
    return 0;
}

void COM_Flush()
{
    unsigned char tmp;
    while(comRead(ComPortNr,(char*)&tmp,1) != 0); //wait till no byte in buffer left
}

unsigned char COM_Reset()
{
    unsigned char ch1,ch2, status;
    unsigned int ms = 0;
    COM_Flush();
    status = COM_sendByte('X'); //Send XX for Reset
    if(status != 0)
    {
        return 1;
    }
    status = COM_sendByte('X');
    if(status != 0)
    {
        return 1;
    }
    while((ms < TIMEOUT) && (COM_getByte(&ch1) != 0));   //wait till answer received or Timeout reached
    {
        ms += 100;
        wait(100);
    }
    ms = 0;
    while((ms < TIMEOUT) && (COM_getByte(&ch2) != 0));
    {
        ms += 100;
        wait(100);
    }
    if(ch1 != 'X' || ch2 != 'X')
    {
        return 2;
    }
    return 0;
}
