/*! \file console.c
    \brief Implementation of Console GUI State Machine
*/

#include "console.h"
#include "parser.h"
#include "utils.h"
#include "string.h"

//Console GUI State Machine
void CONSOLE_run()
{
    static unsigned char CONSOLE_state = 99;   //State Machine variable
    static unsigned char bytes[256];    //Input Buffer
    unsigned char status;               //Status Variable for Return Codes

    switch(CONSOLE_state)
    {
    case 99:   //Init
        printf("----------------------\n");
        printf("| I2CUSB Console App |\n");
        printf("----------------------\n");
        printf("Com Port Number: COM");
        unsigned char portnr;
        scanf("%d",(int*)&portnr);
        status = PARSER_Init(portnr,TAKT_SCL90);   //Init I2C Master Chip
        printf("\nInitializing...");
        if(status != 0)                     //If Init not successful
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 99;             //return to Init menu
            break;
        }
        printf("OK\n");
        CONSOLE_state=0;                    //If successful, go to main menu
        break;
    case 0: //Main Menu
        printf("----------------------\n");
        printf("| I2CUSB Console App |\n");
        printf("----------------------\n");
        printf("1\t-\tTest LED\n");
        printf("2\t-\tTest Relais\n");
        printf("3\t-\tI2C Communication, not working yet\n");
        printf("6\t-\tTest I2C Temperatursensor, working\n");
        printf("7\t-\tTest I2C I/O Expander, working\n");
        printf("8\t-\tDisconnect\n");
        scanf("%d",(int*)&CONSOLE_state);      /*Wait for Input*/
        if(CONSOLE_state > 9)                  //If wrong Input
        {
            printf("wrong input");
            CONSOLE_state = 0;                 //return to main menu
        }
        break;
    case 8: //Disconnect
        printf("Disconnecting...\n");
        CONSOLE_state = 99;                    //return to Init menu
        break;
    case 1: //LED Test
        printf("----------------------\n");
        printf("|      LED Test      |\n");
        printf("----------------------\n");
        printf("LED OFF\n");                   //Switch LED repeatedly
        PARSER_LED_Off();
        wait(500);
        printf("LED ON\n");
        PARSER_LED_On();
        wait(500);
        printf("LED OFF\n");
        PARSER_LED_Off();
        wait(500);
        printf("LED ON\n");
        PARSER_LED_On();
        wait(500);
        printf("LED OFF\n");
        PARSER_LED_Off();
        wait(500);
        printf("LED test successful\n");
        CONSOLE_state = 0;
        break;
    case 2: //Relais Test
        printf("----------------------\n");
        printf("|    Relais Test     |\n");
        printf("----------------------\n");
        printf("Relais OFF\n");                 //Switch Relais repeatedly
        PARSER_Relais_Off();
        wait(500);
        printf("Relais ON\n");
        PARSER_Relais_On();
        wait(500);
        printf("Relais OFF\n");
        PARSER_Relais_Off();
        wait(500);
        printf("Relais ON\n");
        PARSER_Relais_On();
        wait(500);
        printf("Relais OFF\n");
        PARSER_Relais_Off();
        wait(500);
        printf("Relais test successful\n");
        CONSOLE_state = 0;
        break;
    case 3: //I2C Com Test
        printf("----------------------\n");
        printf("| I2C Communication  |\n");
        printf("----------------------\n");
        printf("Syntax: [W AA 55 ...] / [R AA 2]\n");
        printf("Write: 'W' + Address (Hex) + Hex Code of the Bytes\n");
        printf("Read: 'R' + Address (Hex) + Number of Bytes\n");
        printf("Exit: X X X X\n");
        printf("not working yet...\n");
        CONSOLE_state = 0;
        break;  //RETURN

        /*This part was too complicated to be implemented in a driver demo, that won�t be used further.
        It would be a waste of time to parse the user input correctly just for a demo.
        Thats why we just implemented 2 hardcoded tests and not this feature.*/

        //You will never get here...
        static unsigned char bytes_hex[256];        //Input Buffer in Hex
        unsigned char mode = ' ', tmp=' ',address_raw[3];
        unsigned char adr_hex;
        scanf("%c %c %x %s",&tmp,&mode,(unsigned int*)&adr_hex,bytes);      /*Wait for Input, there is an empty char in the beginning of input, discard with tmp variable*/
        if(mode == 'X')
        {
            CONSOLE_state = 0;
            break;
        }
        if(mode == 'W')
        {
            printf("Write Mode\n");
            printf("Address:\t 0x%2x dec:%d\n",adr_hex, adr_hex);
            //printf("Address:\t 0x%2X\n",address);
            printf("Bytes Raw:\t 0x%s\n", bytes);
            unsigned char hex_ptr = 0;
            for(unsigned char i=0; i < strlen((char*)bytes)-1; i+=2) //for each 2 character hex number, leave if only one character left
            {
                unsigned char hex[3];
                hex[0] = *(bytes + i);      //read the 2 character hex number in separate string
                hex[1] = *(bytes + i + 1);
                hex[2] = '\n';
                unsigned char num = (int)strtol((char*)hex, NULL, 16);       // parse string to hex number, returns 0 if not a hex string
                bytes_hex[hex_ptr] = num;                   //save number to array
                hex_ptr++;                                  //increment array pointer
                printf("%d - Hex String:0x%s Number: 0x%x\n",hex_ptr,hex,num);
            }

            bytes_hex[0] = bytes_hex[0];    //remove unused variable warning

            CONSOLE_state = 3;
        }
        else
        {
            printf("input: %c, %s, %s\n",mode,address_raw,bytes);
            CONSOLE_state = 3;
        }
        break;
    case 6: //Test I2C Tempsensor
        printf("----------------------\n");
        printf("| Test I2C Tempsensor|\n");
        printf("----------------------\n");
        printf("Address: 1  0  0  1  0  0  0 [0] -> 0x90\n");   //I2C Address of the Temp Sensor
        wait(500);
        printf("start_i2c(ACK: True, Dest: 0x90, Mode: w)...");
        status = PARSER_start_iic(1,0x90,'w');                         //Start I2C Communication via Master Chip in Write Mode
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);
        printf("wr_byte_iic(0x00)...");
        status = PARSER_wr_byte_iic(0x00);                             //Send 0x00 to select the Temp Register
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);
        printf("restart_i2c(ACK: True, Dest: 0x90, Mode: r)...");
        status = PARSER_restart_iic(1,0x90,'r');                       //restart I2C Communication via Master Chip in Read Mode
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);
        printf("Reading Temp Register from Sensor\n");
        printf("Read first 8 bit\n");
        printf("rd_byte_iic(&read, 0)...");
        unsigned char read;
        status = PARSER_rd_byte_iic(&read, 0);                         //Read the first Byte of the Temp Register
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        printf("Read second 8 bit...");
        unsigned char read2;
        status = PARSER_rd_byte_iic(&read2, 0);                        //read the second byte of the Temp Register
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        printf("stop_iic()...");
        status = PARSER_stop_iic();                                    //Stop the 2C Communication
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);

        printf("Transmission complete!\t Temp Register: 0x%x 0x%x\n",read,read2, read, read2);

        unsigned int temp_16bit = (read << 8) | read2;          //Merge 2 8 bit variables to 16bit
        unsigned int temp_15_7 = (temp_16bit >> 7);             //get Bits 15 to 7 as 8 bit variable
        float temp_celsius = 0.5 * temp_15_7;                   //calculate Temperature in Celsius. 0.5�C per Bit


        printf("First 8 Bit: \t0x%x\nSecond 8 Bit: \t0x%x\n16 Bit: \t0x%x\nBits 15 to 7: \t0x%x\nTemp in Celsius: \t%f\n ",read,read2,temp_16bit,temp_15_7,temp_celsius);
        wait(500);
        CONSOLE_state = 0;                                      //return to main menu
        break;

    case 7: //Test IO Expander
        printf("----------------------\n");
        printf("|  Test IO Expander  |\n");
        printf("----------------------\n");
        printf("Address: 0  1  1  1  0  0  1 [0] -> 0x72\n");   //I2C Address of the IO Expander
        wait(500);
        printf("start_i2c(ACK: True, Dest: 0x72, Mode: w)...");
        status = PARSER_start_iic(1,0x72,'w');                         //Start I2C Communication via Master Chip in Write Mode
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);
        printf("wr_byte_iic(0x00)...");
        status = PARSER_wr_byte_iic(0x00);                             //Write 0x00 to set all Pins to LOW -> All LEDs light up
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);
        printf("wr_byte_iic(0xff)...");
        status = PARSER_wr_byte_iic(0xFF);                             //Write 0xFF to set all Pins to HIGH -> All LEDs turn off
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                                  //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);

        /*We removed the Input part of the IO Expander, as we did not get it to work in time.
        As this is just a driver demo of the I2C Communication and not a full driver implementation of the IO Expander,
        it is not necessary in our opinion.*/

        /*
        printf("restart_i2c(ACK: False, Dest: 0x40, Mode: r)...");
        status = PARSER_restart_iic(0,0x40,'r');
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;
            break;
        }
        printf("OK\t0x%x\n",status);
        printf("Read 8 bit...");
        unsigned char read3;
        status = PARSER_rd_byte_iic(&read3, 0);
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);
        printf("read value:\t0x%02x\n",read3);
        status = PARSER_rd_byte_iic(&read3, 0);
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);
        printf("read value:\t0x%02x\n",read3);

        status = PARSER_rd_byte_iic(&read3, 0);
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;
            break;
        }
        printf("OK\t0x%x\n",status);
        wait(500);
        printf("read value:\t0x%02x\n",read3);
        */

        printf("stop_iic()...");
        status = PARSER_stop_iic();                        //stop the I2C Communication
        if(status == 0xFF)
        {
            printf("failed! Error: %d\n",status);
            CONSOLE_state = 0;                      //if not successful return to main menu
            break;
        }
        printf("OK\t0x%x\n",status);
        printf("Transmission complete!\n");
        wait(500);
        CONSOLE_state = 0;                          //return to main menu
        break;
    }
}
