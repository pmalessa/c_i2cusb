/*! \file main.c
    \brief Main File
*/

#include "console.h"


/*****************************************************************************/
    /* Doxywizard specific */
    /**
    * \mainpage C_I2CUSB
    * \section intro_sec a C Driver Implementation for the USB-ITS-Box
    * <b>Version 1.0 - 06/07/2017</b>
    *
    * This is a port of the provided Delphi Driver for the USB-ITS-Box.
    * This code includes a console GUI Demo for testing purposes.
    * A sample communication is implemented for the Temp Sensor ...
    * and the IO Expander ...
    *
    * This software uses source code from Fr�d�ric Meslin and Florent Touchard,
    * licensed under the MIT license.
    * Therefore this code is also licensed under MIT License.
    *
    * Copyright (c) 2017 Rebecca Plant, Phil Malessa <br>
    */

/**
 * \fn int main()
 * \brief Main Function
 */
int main()
{
    while(1)
    {
        CONSOLE_run();  //run Console GUI App endlessly
    }
    return 0;
}
