/*! \file parser.c
    \brief Implementation of I2C Parser
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "com.h"
#include "parser.h"

/**default Clock: 90 KHz*/
unsigned char Takt = TAKT_SCL90;
/**Global Init variable*/
bool init = 0;

unsigned char PARSER_start_iic(bool MRX_ACK, unsigned char dest, unsigned char mode){ //return 0xFF: PARSER_start_iic unsuccessful
    unsigned char ch1, ch2;
    if(mode == 'w'){ //write on IIC
        COM_Flush();
        COM_sendByte('T');
        COM_sendByte(dest);
        if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
            return 0xFF;
        }
        if(ch1 != 'T'){
            return 0xFF;
        }
        return ch2; //return status
    }
    if(mode == 'r'){ //read from IIC
        if(MRX_ACK == 0){ //read with no ACK
            COM_Flush();
            COM_sendByte('s');
            COM_sendByte(dest);
            if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){ //checking if COM_getByte successful
                return 0xFF;
            }
            if(ch1 != 's'){ //checking if COM_getByte returns correctly
                return 0xFF;
            }
            return ch2; //return status
        }
        if(MRX_ACK == 1){ //read with ACK
            COM_Flush();
            COM_sendByte('S');
            COM_sendByte(dest);
            if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
                return 0xFF;
            }
            if(ch1 != 'S'){
                return 0xFF;
            }
            return ch2; //return status
        }
        return 0xFF; //unsuccessful if ACK unknown
    }
    return 0xFF; //unsuccessful if read/write direction unknown
}


unsigned char PARSER_stop_iic(){
    unsigned char ch1, ch2;
    COM_Flush();
    COM_sendByte('O');
    COM_sendByte('P');
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 3;
    }
    if((ch1 != 'O') || (ch2 != 'P')){
        return 4;
    }
    return 0;
}


unsigned char PARSER_wr_byte_iic(unsigned char b){
    unsigned char ch1, ch2;
    COM_Flush();
    COM_sendByte('N');
    COM_sendByte(b);
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 0xFF;
    }
    if(ch1 != 'N'){
        return 0xFF;
    }
    return ch2;
}


unsigned char PARSER_rd_byte_iic(unsigned char* B, bool NOACK){
    unsigned char ch1, ch2;
    COM_sendByte('R');
    if(NOACK == 0) COM_sendByte('1');
    else COM_sendByte('0');
    if((COM_getByte(&ch1) || COM_getByte(B) || COM_getByte(&ch2)) != 0){
        return 0xFF; //COM_getByte has no value
    }
    if(ch1 != 'R'){
        return 0xFF; //COM_getByte has wrong value
    }
    return ch2;
}


unsigned char PARSER_restart_iic(bool MRX_ACK, unsigned char dest, unsigned char mode){
    unsigned char ch1, ch2;
    if(mode == 'w'){ //write on IIC
        COM_Flush();
        COM_sendByte('U');
        COM_sendByte(dest);
        if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
            return 0xFF;
        }
        if(ch1 != 'U'){
            return 0xFF;
        }
        return ch2; //return status
    }
    if(mode == 'r'){ //read from IIC
        if(MRX_ACK == 0){ //read with no ACK
            COM_Flush();
            COM_sendByte('v');
            COM_sendByte(dest);
            if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){ //checking if COM_getByte successful
                return 0xFF;
            }
            if(ch1 != 'v'){ //checking if COM_getByte returns correctly
                return 0xFF;
            }
            return ch2; //return status
        }
        if(MRX_ACK == 1){ //read with ACK
            COM_Flush();
            COM_sendByte('V');
            COM_sendByte(dest);
            if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
                return 0xFF;
            }
            if(ch1 != 'V'){
                return 0xFF;
            }
            return ch2; //return status
        }
        return 0xFF; //unsuccessful if ACK unknown
    }
    return 0xFF;
}


unsigned char PARSER_wr_byte_port(unsigned char zuSchreiben){
    unsigned char ch1, ch2;
    COM_Flush();
    COM_sendByte('W');
    COM_sendByte(zuSchreiben);
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 3;
    }
    if((ch1 != 'W') || (ch2 != zuSchreiben)){
        return 4;
    }
    return 0;
}


unsigned char PARSER_rd_byte_port (unsigned char gelesen){
    int i=0;
    unsigned char ch1, ch2;
    COM_Flush();
    while(i<2){
        COM_sendByte('D');
        i++;
    }
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 3;
    }
    if(ch1 != 'D'){
        return 4;
    }
    return 0;
}


unsigned char PARSER_Init(unsigned char ComPortNr, unsigned char Takt){
    unsigned char ch1, ch2;
    if(COM_Init(ComPortNr) != 0){
        return 1; //return 1: Com_Init liefert keinen R�ckgabewert
    }
    if((Takt != 'A') && (Takt != 'B') && (Takt != 'C') && (Takt != 'D')){
        return 2; //return 2: Takt nicht bekannt
    }
    COM_Flush();
    COM_sendByte('C');
    COM_sendByte(Takt);
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 3; //return 3: COM_getByte liefert keinen R�ckgabewert
    }
    if((ch1 != 'C') || (ch2 != Takt)){
        return 4; //return 4: COM_getByte liefert falschen R�ckgabewert
    }
    init = 1;
    return 0;
}


bool PARSER_Is_Initialized(){
    if(init == 1){
        return 1; //Is_Initialized TRUE
    }
    return 0; //Is_Initialized FALSE
}


unsigned char PARSER_Relais_On(){
    unsigned char ch1, ch2;
    COM_sendByte('P');
    COM_sendByte('1');
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 3;
    }
    if((ch1 != 'P') || (ch2 != '1')){
        return 4;
    }
    return 0;
}


unsigned char PARSER_Relais_Off(){
    unsigned char ch1, ch2;
    COM_sendByte('P');
    COM_sendByte('0');
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 3;
    }
    if((ch1 != 'P') || (ch2 != '0')){
        return 2;
    }
    return 4;
}


unsigned char PARSER_LED_On(){
    unsigned char ch1, ch2;
    COM_sendByte('L');
    COM_sendByte('1');
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 3;
    }
    if((ch1 != 'L') || (ch2 != '1')){
        return 4;
    }
    return 0;
}


unsigned char PARSER_LED_Off(){
    unsigned char ch1, ch2;
    COM_sendByte('L');
    COM_sendByte('0');
    if((COM_getByte(&ch1) || COM_getByte(&ch2)) != 0){
        return 3;
    }
    if((ch1 != 'L') || (ch2 != '0')){
        return 4;
    }
    return 0;
}
