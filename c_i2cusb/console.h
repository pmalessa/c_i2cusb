/** \file console.h
 *   \brief Header for Console GUI State Machine
*/

#ifndef CONSOLE_H
#define CONSOLE_H

/**
 * \fn void CONSOLE_run()
 * \brief Console GUI State Machine
 */
void CONSOLE_run();

#endif //CONSOLE_H
