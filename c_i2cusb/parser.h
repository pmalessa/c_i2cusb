/*! \file parser.h
    \brief Header of I2C Parser
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "com.h"

/* Definitions for I2C Speed */
/**90 KHz*/
#define TAKT_SCL90 'A'
/**45 KHz*/
#define TAKT_SCL45 'B'
/**11 KHz*/
#define TAKT_SCL11 'C'
/**1.5 KHz*/
#define TAKT_SCL1_5 'D'

/**
 * \fn unsigned char PARSER_Init(unsigned char ComPortNr, unsigned char Takt)
 * \brief Sets bus clock, initializes and checks communication with I2C box
 * \param[in] ComPortNr Number of COM-Port
 * \param[in] Takt Bus clock frequency
 * \return 0 successful
 * \return 1 COM_Init has no value
 * \return 2 clock unknown
 * \return 3 COM_getByte has no value
 * \return 4 COM_getByte has wrong value
 */
unsigned char PARSER_Init(unsigned char ComPortNr, unsigned char Takt);

/**
 * \fn unsigned char PARSER_Relais_On()
 * \brief Switches on relay inside I2C box
 * \return 0 successful
 * \return 1 not successful
 */
unsigned char PARSER_Relais_On();

/**
 * \fn unsigned char PARSER_Relais_Off()
 * \brief Switches off relay inside I2C box
 * \return 0 successful
 * \return 1 not successful
 */
unsigned char PARSER_Relais_Off();

/**
 * \fn unsigned char PARSER_LED_On()
 * \brief Switches on LED on I2C box
 * \return 0 successful
 * \return 1 not successful
 */
unsigned char PARSER_LED_On();

/**
 * \fn unsigned char PARSER_LED_Off()
 * \brief Switches off LED on I2C box
 * \return 0 successful
 * \return 1 not successful
 */
unsigned char PARSER_LED_Off();

/**
 * \fn unsigned char PARSER_wr_byte_port(unsigned char zuSchreiben)
 * \brief Writes one byte on I/O port of I2C box
 * \param[in] zuSchreiben Byte to be written on I/O port
 * \return 0 successful
 * \return 1 not successful
 */
unsigned char PARSER_wr_byte_port(unsigned char zuSchreiben);

/**
 * \fn unsigned char PARSER_rd_byte_port(unsigned char gelesen)
 * \brief Reads one byte from I/O port of I2C box
 * \param[in] gelesen Byte read from I/O port
 * \return 0 successful
 * \return 1 not successful
 */
unsigned char PARSER_rd_byte_port (unsigned char gelesen);

/**
 * \fn unsigned char PARSER_start_iic(bool MRX_ACK, unsigned char dest, unsigned char mode)
 * \brief Sends start condition with destination address: write/read with/whithout acknowledge
 * \param[in] MRX_ACK 1 if acknowledge wanted, 0 else
 * \param[in] dest Address of addressed component
 * \param[in] mode Write (w) or read (r)
 * \return status byte if successful
 * \return 0xFF not successful
 */
unsigned char PARSER_start_iic(bool MRX_ACK, unsigned char dest, unsigned char mode);

/**
 * \fn unsigned PARSER_stop_iic()
 * \brief Sends stop condition in I2C bus
 * \return 0 successful
 * \return 3 COM_getByte has no value
 * \return 4 COM_getByte has wrong value
 */
unsigned char PARSER_stop_iic();

/**
 * \fn unsigned char PARSER_wr_byte_iic(unsigned char b)
 * \brief Writes byte on I2C bus
 * \param[in] b Byte to be sent
 * \return status byte if successful
 * \return 0xFF not successful
 */
unsigned char PARSER_wr_byte_iic(unsigned char b);

/**
 *\fn unsigned char PARSER_rd_byte_iic(unsigned char* B, bool NOACK)
 *\brief Reads byte from I2C bus, determines ACK or NO ACK for next transfer
 *\param[in] B space for read byte
 *\param[in] NOACK 0 if acknowledge wanted, 1 else
 * \return status byte if successful
 * \return 0xFF not successful
 */
unsigned char PARSER_rd_byte_iic( unsigned char* B, bool NOACK);

/**
 * \fn unsigned char PARSER_restart_iic(bool MRX_ACK, unsigned char dest, unsigned char mode)
 * \brief Sends repeated start condition with destination address: write/read with/whithout acknowledge
 * \param[in] MRX_ACK 1 if acknowledge wanted, 0 else
 * \param[in] dest Address of addressed component
 * \param[in] mode Write (w) or read (r)
 * \return status byte if successful
 * \return 0xFF not successful
 */
unsigned char PARSER_restart_iic(bool MRX_ACK, unsigned char dest, unsigned char mode);

/**
 * \fn bool PARSER_Is_Initialized()
 * \brief Checks initialization status of I2C box
 * \return 1 Initialized
 * \return 0 not Initialized
 */
bool PARSER_Is_Initialized();
